<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Objects" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <%@ include file="header.jsp" %>
</head>
<body>
<form action="todo_session.jsp">
    <label> Item:
        <input name="item" required>
    </label>
</form>
<ul>
    <%
        List<String> todo = (List<String>) session.getAttribute("items");
        if (todo == null) {
            todo = new ArrayList<String>();
            session.setAttribute("items", todo);
        }
        String item = request.getParameter("item");
        if (item != null && !item.equals("")) {
            todo.add(item);
        }

        for (String s : todo) {
            out.println("<li>" + s + "</li>");
        }
    %>
</ul>

</body>
</html>
