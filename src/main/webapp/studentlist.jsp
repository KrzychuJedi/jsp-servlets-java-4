<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="org.sda.domain.Student" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>

<%!
    List<Student> students = new ArrayList<>();
%>

<%
    students.clear();
    students.add(new Student("Jan", "Kowalski", true));
    students.add(new Student("Karol", "Kaczmarek", true));
    students.add(new Student("Staś", "Trudny", false));
    pageContext.setAttribute("students", students);
//    request.setAttribute("students", students);
//    session.setAttribute("students", students);
//    application.setAttribute("students", students);
%>
<html>
<head>
    <title>Title</title>
    <%--<style>--%>
    <%--td, tr, table, th {--%>
    <%--border: solid black;--%>
    <%--}--%>
    <%--</style>--%>
    <%@ include file="header.jsp" %>
</head>
<body>
<table class="table table-bordered table-striped">
    <tr>
        <th>
            Name
        </th>
        <th>
            Surname
        </th>
        <th>
            Passed
        </th>
    </tr>
    <%
        for (Student student : students) {
            out.println("<tr>");
            out.print("<td>" + student.getFirstName() + "</td>");
            out.print("<td>" + student.getLastName() + "</td>");
            out.print("<td>" + student.isPassed() + "</td>");
            out.println("</tr>");
        }
    %>

    <c:forEach items="${students}" var="student">
        <tr>
            <td>
                    ${student.firstName}
            </td>
            <td>
                    ${student.lastName}
            </td>
            <td>
                <%--<c:if test="${student.passed}">--%>
                    <%--Passed!--%>
                <%--</c:if>--%>
                <%--<c:if test="${not student.passed}">--%>
                    <%--:(--%>
                <%--</c:if>--%>

                <c:choose>
                    <c:when test="${student.passed}">
                        Passed! x
                    </c:when>
                    <c:otherwise>
                        :( y
                    </c:otherwise>
                </c:choose>
            </td>
        </tr>
    </c:forEach>
</table>

<%--<c:forEach >--%>
<%----%>
<%--</c:forEach>--%>

</body>
</html>
