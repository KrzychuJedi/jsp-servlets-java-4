<%@ page import="java.util.Date" %>
<%@ page import="org.sda.jsp.JspUtil" %>
<%@ page import="java.util.Enumeration" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%!

    // TODO dodaj kod który pobierze godzinę o której JSP jest zainicjalizowane
    public void jspInit() {
    }

    // TODO wyświetl informację o której godzinie była inicjalizacja i jaka jest godzina teraz

    public void jspDestroy() {
    }
%>

<html>
<head>
    <title>$Title$</title>
    <%@ include file="header.jsp" %>
</head>
<body>

<ul class="nav nav-tabs">
    <li role="presentation" class="active"><a href="index.jsp">Home</a></li>
    <li role="presentation"><a href="todo.jsp">TODO</a></li>
    <li role="presentation"><a href="todo_session.jsp">TODO Session</a></li>
    <li role="presentation"><a href="mailform.jsp">Mail form</a></li>
    <li role="presentation"><a href="mailform_wyswig.jsp">Mail form WYSIWG</a></li>
    <li role="presentation"><a href="cookis.jsp">Cookie add</a></li>
    <li role="presentation"><a href="cookieShow.jsp">Cookie show</a></li>
    <li role="presentation"><a href="studentlist.jsp">Lista studentów - forEach</a></li>
    <li role="presentation"><a href="functions.jsp">Funkcje</a></li>
</ul>

<div class="container" style="margin-top: 20px">

    <%-- TODO Hello World i aktualna godzina--%>
    <%= JspUtil.getUpperCase("Hello World") %> <br>
    <%= new Date() %> <br>

    Aktualna data to:
    <br/>
    25 razy 4 to:
    <%out.println(25 * 4);%>
    <br/>
    Czy 50 jest mniejsze od 21?
    <%out.print(50 < 21);%>
    <br/>

    <%-- TODO zmienne zdefiniowane --%>
    Przeglądarka użytkownika:
    <%=request.getHeader("User-Agent")%>
    <br/>
    Język zapytania:
    <%=request.getLocale()%>
    <br/>
    Lista wszystkich nagłówków zapytania
    <ol>
        <%
            Enumeration<String> headers = request.getHeaderNames();
            while (headers.hasMoreElements()) {
                String header = headers.nextElement();
                out.println("<li>");
                out.println(header);
                out.println(" : ");
                String value = request.getHeader(header);
                out.println(value);
                out.println("</li>");
            }
        %>
    </ol>


</div>

<form>

    <select name="language">
        <option>JAVA</option>
        <option>C++</option>
        <option>Ruby</option>
    </select>

    <input type="radio" name="Age" value="10"> 10
    <input type="radio" name="Age" value="11"> 11

    <input type="checkbox" name="sex" value="10"> Male
    <input type="checkbox" name="sex" value="11"> Female

</form>

<%-- TODO Obsługa formy --%>
<%-- TODO Dodać <select> checkbox i radiobutton --%>

<div class="container" style="margin-top: 20px">
    <form action="formresponse.jsp">
        <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                   placeholder="Enter email" name="email">
            <small id="emailHelp" class="form-text text-muted">Nigdy nie udostepnimy Twojego e-maila</small>
        </div>
        <div class="form-group">
            <label for="imie">Imie</label>
            <input type="text" class="form-control" name="name" id="imie" placeholder="Imie">
        </div>
        <div class="form-group">
            <label for="nazwisko">Nazwisko</label>
            <input type="text" class="form-control" name="surname" id="nazwisko" placeholder="Nazwisko">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>

<div class="container">
    <%@ include file="footer.jsp" %>
</div>


</body>
</html>
