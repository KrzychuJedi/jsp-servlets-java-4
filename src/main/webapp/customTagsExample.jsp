<%@ page import="org.sda.domain.Student" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%
    List<Student> students = new ArrayList<>();
    students.add(new Student("Jan", "Kowalski", true));
    students.add(new Student("Karol", "Kaczmarek", true));
    students.add(new Student("Staś", "Trudny", false));
    pageContext.setAttribute("students", students);
%>
<html>
<head>
    <title>Title</title>
</head>
<body>
<c:forEach items="${students}" var="studentx">
    <t:custom student="${studentx}"></t:custom>
    <hr>
</c:forEach>
<%
    for(Student studentx: students){

    }
%>
</body>
</html>
