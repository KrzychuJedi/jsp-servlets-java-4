<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%
    String localeParam = request.getParameter("locale");
    if (localeParam != null && !localeParam.equals("")) {
        Cookie newCookie = new Cookie("locale", localeParam);
        newCookie.setMaxAge(60 * 60 * 24 * 365);
        response.addCookie(newCookie);
    }
%>

<c:set var="language" value="${not empty param.locale ? param.locale : 'pl_PL'}" scope="session" />

<%--<fmt:setLocale value="pl_PL"/>--%>
<fmt:setLocale value="${cookie.locale.value}"/>
<%--<fmt:setLocale value="${language}"/>--%>

<fmt:setBundle basename="org.sda.labels.translation"/>

<html>
<head>
    <title>Title</title>
</head>
<body>
<fmt:message key="label.greeting"/>

<fmt:message key="label.someText"/>
</body>
</html>
