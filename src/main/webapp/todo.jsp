<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Objects" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <%@ include file="header.jsp" %>
</head>
<%!
    List<String> todo = new ArrayList<>();
%>
<body>
<form action="todo.jsp">
    <label> Item:
        <input name="item" required>
    </label>
</form>
<ul>
    <%
        String item = request.getParameter("item");
        if (item != null && !item.equals("")) {
            todo.add(item);
        }

        for (String s : todo) {
            out.println("<li>" + s + "</li>");
        }
    %>
</ul>

</body>
</html>
