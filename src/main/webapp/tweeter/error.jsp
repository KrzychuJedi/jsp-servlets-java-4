<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
ERROR

<c:forEach items="${errors}" var="error">
    ${error.getPropertyPath()} :
    ${error.getMessage()}
</c:forEach>
</body>
</html>
