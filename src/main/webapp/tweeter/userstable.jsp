<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <%@ include file="../header.jsp" %>
</head>
<body>
<div class="container" style="margin-top: 20px">
    <table class="table table-bordered table-striped">
        <tr>
            <th>
                Login
            </th>
            <th>
                First Name
            </th>
            <th>
                Last Name
            </th>
            <th>
                E-mail
            </th>
            <th>
                Actions
            </th>
        </tr>


        <c:forEach items="${users}" var="user">
            <tr>
                <td>
                        ${user.login}
                </td>
                <td>
                        ${user.firstName}
                </td>
                <td>
                        ${user.lastName}
                </td>
                <td>
                        ${user.email}
                </td>
                <td>
                    <form action="/edituser" method="post">
                        <input type="hidden" value="${user.id}" name="id">
                        <input type="submit" value="Edytuj">
                    </form>
                    <form action="/deleteuser" method="post">
                        <input type="hidden" value="${user.id}" name="id">
                        <input type="submit" value="Usuń">
                    </form>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>
