<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: krzysztof.gonia
  Date: 10/11/2017
  Time: 8:20 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<%
    List<String> anotherList = new ArrayList<>();
    anotherList.add("New York");
    anotherList.add("Sidney");
    anotherList.add("Błędzin");
    pageContext.setAttribute("someList", anotherList);
%>
<body>
<c:set var="languages" value="${['Wrocław','Gdańsk','Poznań']}"/>
<form>
    <select name="city">
        <c:forEach items="${languages}" var="city">
            <option>${city}</option>
        </c:forEach>
        <c:forEach items="${someList}" var="city">
            <option>${city}</option>
        </c:forEach>
    </select>
    <input type="submit" value="submit">
</form>
</body>
</html>
