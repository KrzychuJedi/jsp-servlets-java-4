<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<ul>
    <c:forEach items="${header.keySet()}" var="key">
        <li>${key} : ${header.get(key)}</li>
    </c:forEach>
</ul>

<form>
    <select name="header">
        <c:forEach items="${header.keySet()}" var="key">
            <option>${key} : ${header.get(key)}</option>
        </c:forEach>
    </select>
</form>
</body>
</html>
