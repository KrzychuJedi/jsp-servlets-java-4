<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--<%!--%>
    <%--String sda = new String("Software Development Academy");--%>
<%--%>--%>
<c:set var="sda" value="Software Development Academy"/>
<c:set var="myString" value="Wrocław,Warszawa,Gdańsk,Bydgoszcz"/>
<html>
<head>
    <title>Title</title>
</head>
<body>
Długość <b>${sda}</b> : ${fn:length(sda)}
<br>
Uppercase: ${fn:toUpperCase(sda)}
<br>
Czy zaczyna się od 'Software' : ${fn:startsWith(sda, "Software")}
<br>
Czy zaczyna się od 'Academy' : ${fn:startsWith(sda, "Academy")}
<br>
${myString}
<br>
<c:set var="cityList" value="${fn:split(myString,',')}"/>
<c:forEach items="${cityList}" var="city">
    ${city}<br>
</c:forEach>
<br>
<c:set var="joinedList" value="${fn:join(cityList,' ')}"/>
${joinedList}



</body>
</html>
