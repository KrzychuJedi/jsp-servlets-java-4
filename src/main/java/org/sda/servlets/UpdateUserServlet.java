package org.sda.servlets;

import org.sda.domain.User;
import org.sda.services.IUserService;
import org.sda.services.impl.UserServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.http.HTTPException;
import java.io.IOException;

@WebServlet(value = "/updateuserservlet")
public class UpdateUserServlet extends HttpServlet {

    private IUserService userService = new UserServiceImpl();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String firstName = req.getParameter("firstname");
        String lastName = req.getParameter("lastname");
        String email = req.getParameter("email");
        int id = Integer.valueOf(req.getParameter("id"));

        User user = userService.getBy(id);
        user.setLogin(login);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(email);

        userService.update(user);

        req.setAttribute("users", userService.findAll());

        RequestDispatcher requestDispatcher = req.getRequestDispatcher("tweeter/userstable.jsp");
        requestDispatcher.forward(req, resp);

    }
}
