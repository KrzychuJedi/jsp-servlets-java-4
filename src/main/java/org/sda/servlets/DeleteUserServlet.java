package org.sda.servlets;

import org.sda.services.IUserService;
import org.sda.services.impl.UserServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(value = "/deleteuser")
public class DeleteUserServlet extends AbstractServlet {

    private IUserService userService = new UserServiceImpl();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = Integer.valueOf(req.getParameter("id"));
        userService.delete(id);

        //option 2 - copy
        req.setAttribute("users", userService.findAll());

        RequestDispatcher requestDispatcher = req.getRequestDispatcher("tweeter/userstable.jsp");
        requestDispatcher.forward(req, resp);

        //option 3 - forward
//        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/userslist");
//        requestDispatcher.forward(req, resp);

        //option 1 - abstract
//        redirectToUserPage(req, resp);


    }
}
