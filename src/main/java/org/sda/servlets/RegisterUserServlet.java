package org.sda.servlets;

import org.sda.domain.Password;
import org.sda.domain.User;
import org.sda.services.IUserService;
import org.sda.services.impl.UserServiceImpl;
import org.sda.util.PasswordUtil;
import org.sda.util.ValidationUtil;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import javax.validation.ConstraintViolation;
import java.io.IOException;
import java.util.Set;

@WebServlet(value = "/register")
public class RegisterUserServlet extends HttpServlet {

    @Resource(name = "jdbc/sda_DB")
    DataSource ds;

    private IUserService userService;

    @Override
    public void init() throws ServletException {
        userService = new UserServiceImpl(ds);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User newUser = new User(
                req.getParameter("login"),
                req.getParameter("firstname"),
                req.getParameter("lastname"),
                req.getParameter("email")
        );

        String passwordParam = req.getParameter("password");
        Password password = new Password();
        password.setValue(PasswordUtil.hashPassword(passwordParam));
        password.setUser(newUser);

        Set<ConstraintViolation<User>> constraintViolations = ValidationUtil.validateInternal(newUser);
        Set<ConstraintViolation<Password>> constraintViolationsPassword = ValidationUtil.validateInternal(password);

        if(constraintViolations.isEmpty() && constraintViolationsPassword.isEmpty()) {
            User user = userService.save(newUser, password);
            req.setAttribute("user", user);

            RequestDispatcher requestDispatcher = req.getRequestDispatcher("/tweeter/userpage.jsp");
            requestDispatcher.forward(req, resp);
        } else {
            req.setAttribute("errors", constraintViolations);

            for(ConstraintViolation<User> violation : constraintViolations){
                System.out.println(violation.getPropertyPath());
                System.out.println(violation.getMessage());
            }

            RequestDispatcher requestDispatcher = req.getRequestDispatcher("/tweeter/error.jsp");
            requestDispatcher.forward(req, resp);
        }
    }
}
