package org.sda.servlets;

import org.sda.domain.Password;
import org.sda.domain.User;
import org.sda.services.IUserService;
import org.sda.services.impl.UserServiceImpl;
import org.sda.util.PasswordUtil;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import java.io.IOException;

@WebServlet(value = "/loguser")
public class LoginServlet extends HttpServlet {

    @Resource(name = "jdbc/sda_DB")
    DataSource ds;

    private IUserService userService;

    @Override
    public void init() throws ServletException {
        userService = new UserServiceImpl(ds);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String passwordParam = req.getParameter("password");

        User user = userService.getByLogin(login);
        Password password = userService.getPassword(user);

        if(password != null && PasswordUtil.checkPassword(passwordParam,password.getValue())) {
            HttpSession session = req.getSession();
            session.setAttribute("loggedInUser", user);

            RequestDispatcher requestDispatcher = req.getRequestDispatcher("tweeter/tweets.jsp");
            requestDispatcher.forward(req, resp);
        } else {
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("tweeter/login.jsp");
            requestDispatcher.forward(req, resp);
        }
    }
}
