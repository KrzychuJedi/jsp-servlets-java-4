package org.sda.servlets;

import org.sda.services.IUserService;
import org.sda.services.impl.UserServiceImpl;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;

@WebServlet(value = "/userslist")
public class UsersServlet extends HttpServlet{

    @Resource(name = "jdbc/sda_DB")
    DataSource ds;

    private IUserService userService;

    @Override
    public void init() throws ServletException {
        userService = new UserServiceImpl(ds);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("users", userService.findAll());

        RequestDispatcher requestDispatcher = req.getRequestDispatcher("tweeter/userstable.jsp");
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("users", userService.findAll());

        RequestDispatcher requestDispatcher = req.getRequestDispatcher("tweeter/userstable.jsp");
        requestDispatcher.forward(req, resp);
    }
}
