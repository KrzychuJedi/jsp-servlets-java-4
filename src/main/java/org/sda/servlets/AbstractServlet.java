package org.sda.servlets;

import org.sda.services.IUserService;
import org.sda.services.impl.UserServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AbstractServlet extends HttpServlet {

    public IUserService userService = new UserServiceImpl();

    public void redirectToUserPage(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("users", userService.findAll());

        RequestDispatcher requestDispatcher = req.getRequestDispatcher("tweeter/userstable.jsp");
        requestDispatcher.forward(req, resp);
    }
}
