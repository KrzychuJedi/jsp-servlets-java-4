package org.sda.przypomnienie;

import java.util.List;

public class Test {

    private static class Dog {
        public String voice;
        public String getVoice() {
            return voice;
        }
        public void setVoice(String voice) {
            this.voice = voice;
        }
    }

    public static void main(String[] args) {
        Dog pimpus = new Dog();
        pimpus.setVoice("Hau Hau!");
        pimpus = new Dog();
        test(pimpus);
        System.out.println(pimpus.getVoice());
    }

    public static void test(Dog dog) {
//        dog.setVoice("Miau!");
        dog = new Dog();
        dog.setVoice("Kici kici");
    }
}
