package org.sda.services;

import org.sda.domain.Password;
import org.sda.domain.User;

import java.util.List;

public interface IUserService {
    User save(User user, Password password);
    List<User> findAll();
    void delete(int id);
    User getBy (int id);
    void update(User user);
    Password getPassword(User user);
    User getByLogin(String login);
}
