package org.sda.services.impl;

import org.sda.dao.IPasswordDao;
import org.sda.dao.IUserDao;
import org.sda.dao.impl.PasswordDaoDb;
import org.sda.dao.impl.PasswordDaoImpl;
import org.sda.dao.impl.UserDaoDb;
import org.sda.dao.impl.UserDaoImpl;
import org.sda.domain.Password;
import org.sda.domain.User;
import org.sda.services.IUserService;

import javax.sql.DataSource;
import javax.transaction.Transactional;
import java.util.List;

public class UserServiceImpl implements IUserService {

    private IUserDao userDao;

    private IPasswordDao passwordDao;

    public UserServiceImpl() {
    }

    public UserServiceImpl(DataSource ds) {
        userDao= new UserDaoDb(ds);
        passwordDao= new PasswordDaoDb(ds);
    }

    @Override
//    @Transactional
    public User save(User user, Password password) {
        User savedUser = userDao.save(user);
        passwordDao.save(password);
        return savedUser;
    }

    @Override
    public Password getPassword(User user){
        return passwordDao.getBy(user);
    }

    @Override
    public User getByLogin(String login) {
        return userDao.getByLogin(login);
    }

    @Override
    public List<User> findAll() {
        return userDao.findAll();
    }

    @Override
    public void delete(int id) {
        userDao.delete(id);
    }

    @Override
    public User getBy(int id) {
        return userDao.getBy(id);
    }

    public void update(User user){
        userDao.update(user);
    }
}
