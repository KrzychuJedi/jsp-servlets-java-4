package org.sda.dao.impl;

import org.sda.dao.IPasswordDao;
import org.sda.domain.Password;
import org.sda.domain.User;
import org.sda.util.DbUtil;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PasswordDaoDb implements IPasswordDao {

    private final DataSource ds;

    public PasswordDaoDb(DataSource ds) {
        this.ds = ds;
    }

    @Override
    public void save(Password password) {
        try {
            Connection con = ds.getConnection();
            String sql = "INSERT INTO password(value,user_id) VALUES (?,?)";
            PreparedStatement statement = con.prepareStatement(sql);
            statement.setString(1, password.getValue());
            statement.setInt(2, password.getUser().getId());
            statement.executeUpdate();
            DbUtil.close(con, statement, null);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
        }
    }

    @Override
    public Password getBy(User user) {
        Password password = new Password();
        try {
            Connection con = ds.getConnection();
            String sql = "select * from password where user_id = ?";
            PreparedStatement statement = con.prepareStatement(sql);
            statement.setInt(1, user.getId());
            ResultSet rs = statement.executeQuery();
            if(rs.next()) {
                password.setValue(rs.getString("value"));
                password.setId(rs.getInt("id"));
                password.setUser(user);
            }
            DbUtil.close(con, statement, rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return password;
    }
}
