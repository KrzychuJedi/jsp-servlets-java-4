package org.sda.dao.impl;

import org.sda.dao.IUserDao;
import org.sda.domain.User;

import java.util.ArrayList;
import java.util.List;

public class UserDaoImpl implements IUserDao {

    private static List<User> users = new ArrayList<>();

    private static int counter = 0;

    @Override
    public User save(User user) {
        users.add(user);
        user.setId(counter++);
        return user;
    }

    @Override
    public List<User> findAll() {
        return users;
    }

    @Override
    public void delete(int id) {
        User toDelete = getBy(id);
        if(toDelete != null) {
            users.remove(toDelete);
        }
    }

    // can return optional
    @Override
    public User getBy(int id){
        for(User user: users){
            if(user.getId() == id){
                return user;
            }
        }
        return null;
    }

    @Override
    public void update(User user) {
    }

    @Override
    public User getByLogin(String login) {
        for(User user: users){
            if(user.getLogin().equals(login)){
                return user;
            }
        }
        return null;
    }
}
