package org.sda.dao.impl;

import org.sda.dao.IPasswordDao;
import org.sda.domain.Password;
import org.sda.domain.User;

import java.util.HashMap;
import java.util.Map;

public class PasswordDaoImpl implements IPasswordDao {

    private static Map<User, Password> passwordMap = new HashMap();

    private static int counter = 0;

    @Override
    public void save(Password password) {
        passwordMap.put(password.getUser(), password);
        password.setId(++counter);
    }

    @Override
    public Password getBy(User user) {
        return passwordMap.get(user);
    }


}
