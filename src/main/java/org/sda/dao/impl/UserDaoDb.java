package org.sda.dao.impl;


import org.sda.dao.IUserDao;
import org.sda.domain.Password;
import org.sda.domain.User;
import org.sda.util.DbUtil;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDaoDb implements IUserDao {

    private final DataSource ds;

    public UserDaoDb(DataSource ds) {
        this.ds = ds;
    }

    @Override
    public User save(User user) {
        try {
            Connection con = ds.getConnection();
            String sql = "INSERT INTO user(login,first_name,last_name,email) VALUES (?,?,?,?)";
            PreparedStatement statement = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getFirstName());
            statement.setString(3, user.getLastName());
            statement.setString(4, user.getEmail());
            statement.executeUpdate();
            ResultSet rs = statement.getGeneratedKeys();
            rs.next();
            user.setId(rs.getInt(1));
            DbUtil.close(con, statement, rs);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
        }
        return user;
    }

    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        try {
            Connection con = ds.getConnection();
            String select = "select * from user";
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(select);
            while (rs.next()) {
                User user = new User();
                user.setLogin(rs.getString("login"));
                user.setFirstName(rs.getString("first_name"));
                user.setLastName(rs.getString("last_name"));
                user.setEmail(rs.getString("email"));
                user.setId(rs.getInt("id"));
                users.add(user);
            }
            DbUtil.close(con, stmt, rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }

    @Override
    public void delete(int id) {

    }

    @Override
    public User getBy(int id) {
        return null;
    }

    @Override
    public void update(User user) {

    }

    @Override
    public User getByLogin(String login) {
        User user = new User();
        try {
            Connection con = ds.getConnection();
            String sql = "select * from user where login = ?";
            PreparedStatement statement = con.prepareStatement(sql);
            statement.setString(1, login);
            ResultSet rs = statement.executeQuery();
            if(rs.next()) {
                user.setLogin(rs.getString("login"));
                user.setFirstName(rs.getString("first_name"));
                user.setLastName(rs.getString("last_name"));
                user.setEmail(rs.getString("email"));
                user.setId(rs.getInt("id"));
            }
            DbUtil.close(con, statement, rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

}
