package org.sda.dao;

import org.sda.domain.User;

import java.util.List;

public interface IUserDao {

    User save(User user);
    List<User> findAll();
    void delete(int id);

    User getBy(int id);

    void update(User user);

    User getByLogin(String login);
}
