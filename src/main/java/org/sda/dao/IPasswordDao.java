package org.sda.dao;

import org.sda.domain.Password;
import org.sda.domain.User;

public interface IPasswordDao {

    void save(Password password);

    Password getBy(User user);
}
