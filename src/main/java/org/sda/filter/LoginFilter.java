package org.sda.filter;

import org.sda.domain.User;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebFilter("/*")
public class LoginFilter implements Filter {

    private List<String> whiteList = new ArrayList<>();

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        whiteList.add("/tweeter/registeruser.jsp");
        whiteList.add("/register");
        whiteList.add("/loguser");
        whiteList.add("/db");
//        whiteList.add("/userslist");
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        HttpSession session = request.getSession();

        User loggedInUser = (User) session.getAttribute("loggedInUser");
        if (loggedInUser != null || whiteList.contains(request.getRequestURI())) {
            chain.doFilter(request, response);
        } else {
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("/tweeter/login.jsp");
            requestDispatcher.forward(request, response);
        }
    }

    @Override
    public void destroy() {

    }
}
