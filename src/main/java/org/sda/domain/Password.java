package org.sda.domain;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class Password {

    private int id;

    @NotBlank
    private String value;

    @NotNull
    private User user;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
