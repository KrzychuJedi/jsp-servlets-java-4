package org.sda.domain;

public class Student {

    private String firstName;
    private String lastName;
    private boolean passed;

    public Student(String firstName, String lastName, boolean passed) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.passed = passed;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public boolean isPassed() {
        return passed;
    }

    public void setPassed(boolean passed) {
        this.passed = passed;
    }
}
