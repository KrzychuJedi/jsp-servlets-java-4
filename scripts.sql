CREATE USER 'sda_user'@'localhost' IDENTIFIED BY 'sda_pass';
GRANT ALL PRIVILEGES ON *.* TO 'sda_user'@'localhost';

CREATE SCHEMA `sda_db` ;

CREATE TABLE `employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `sda_db`.`employee` (`name`) VALUES ('Jan');
INSERT INTO `sda_db`.`employee` (`name`) VALUES ('Karol');


CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` VARCHAR(20) UNIQUE NOT NULL,
  `first_name` VARCHAR(30) NOT NULL,
  `last_name` VARCHAR(30) NOT NULL,
  `email` VARCHAR(255) UNIQUE NOT NULL,
  PRIMARY KEY (`id`)
)

CREATE TABLE `password` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` VARCHAR(32) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (user_id) REFERENCES user(id) CASCADE ON UPDATE DELETE
)


